# android gitlab-ci

## pipeline

`.gitlab-ci.yml` состоит из этапов `stage` в каждом из котором содержит задачи из `jobs`

[Пример](https://gitlab.com/el_mail/android-gitlab-ci/-/pipelines/330202324)

## stage

**environment:** подготовительная сборка среды;

**build:** сборка приложения;

**testing:** автотесты;

**internal:** развёртывание приложения для Разработчиков, Тестировщиков;

**alpha:** релиз в «alpha» для команды Тестировщиков;

**beta:** релиз в beta для команды Тестировщиков и небольшой аудиторий клиентов;

**production:** релиз на production.

## jobs

## Как это используется?

При каждом `git push` в ветки с префиксом `feat/` запускает этапы: `build, testing, staging`


Пример использования:
- `git branch feat/notification` - ветка с которой работает Разработчик


теги с префиксом `v` или `rc` запускает этапы: `build, testing, staging, pre-production, approve, production`

Пример использования:
- `git tag v1.0.1` - релиз версия
- `git tag rc-07.07.2021` - релиз кандидат
