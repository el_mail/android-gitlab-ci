#FROM python:alpine
# precompiled python wheels for pandas and numpy => faster docker build
FROM nickgryg/alpine-pandas
#==================================
# Fix Issue with timezone mismatch
#==================================
ENV TZ="Asia/Almaty"
ENV http_proxy="http://0.0.0.0:9090"
ENV https_proxy="https://0.0.0.0:9090"
ENV DESIRED_CAPABILITIES=SGS10-9.0-1440x3040-560-emu-by-apk-relapath.json
ENV APK_FULLPATH=/app/app-bankTest-x86-debug.apk
ENV LOG_LEVEL=DEBUG
ENV TESTS_TO_RUN=tests/test_login.py
ENV UPLOAD_ALLURE_RESULTS=false
ENV ALLURE_RESULTS=allure-results/
ENV ALLURE_DEST_DIR=/media/nfs/ala333230/mnt/upload
RUN echo "${TZ}" > /etc/timezone
# install dependencies
# the lapack package is only in the community repository
RUN echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
# Install dependencies
RUN apk add --no-cache --virtual .build-deps \
    gfortran \
    musl-dev \
    g++
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h
RUN apk --update add --no-cache \
    freetype-dev \
    lapack-dev \
    gcc \
    poppler \
    poppler-dev \
    libjpeg \
    zlib \
    build-base \
    jpeg-dev \
    zlib-dev \
    bash \
    rsync
# removing dependencies to limit the size of your image
#RUN apk del .build-deps
#RUN apk add python py-pip python-dev
WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
COPY . /app
# Appium -> Android (PyTest & Appium build&run in the same one container)
# EXPOSE 8200
# PyTest -> Appium (PyTest & Appium build&run in separate containers)
#EXPOSE 4723
# RUN cat /etc/alpine-release
#RUN python --version
#ENTRYPOINT ["pytest"]
RUN find /app -iname "*.apk"
CMD pytest -srAv --desired-capabilities=$DESIRED_CAPABILITIES \
        --apk-fullpath=$APK_FULLPATH \
        --apk-name=$APK_NAME \
        --alluredir=$ALLURE_RESULTS \
        --log-level=$LOG_LEVEL \
        --screen-rec-by-test \
        --pin-code-popups \
        $TESTS_TO_RUN; \
        if $UPLOAD_ALLURE_RESULTS; then rsync -rh --progress --size-only \
        $ALLURE_RESULTS $ALLURE_DEST_DIR; fi